CONVERT = pandoc
FLAGS = 
TEXFLAGS = -V documentclass=memoir -V margin-left=2cm -V margin-right=2cm -V fontsize=14pt
BOOK_FILENAME = Book
METADATA = ./metadata.yml
CONT_DIR = ./content
BUILD_DIR = ./build
CONTENT = $(sort $(wildcard $(CONT_DIR)/*.org))
BUILD = $(CONTENT:$(CONT_DIR)/%.org=$(BUILD_DIR)/%.md)
EBOOKFORMATS = epub html fb2
PRINTFORMATS = tex pdf
EXTARFORMATS =

all: ebook print extra

ebook: $(EBOOKFORMATS)

print: $(PRINTFORMATS)

extra: $(EXTRAFORMATS)

mobi: epub

$(EBOOKFORMATS): %: $(BOOK_FILENAME).%

$(PRINTFORMATS): %: $(BOOK_FILENAME).%

$(EXTRAFORMATS): %: $(BOOK_FILENAME).%

$(BOOK_FILENAME).mobi: $(BOOK_FILENAME).epub
	kindlegen $^ -o $@

$(BOOK_FILENAME).%: $(BUILD) $(METADATA)
	$(CONVERT) $(FLAGS) $(TEXFLAGS) $^ -o $@

$(BUILD): $(BUILD_DIR)/%.md: $(CONT_DIR)/%.org $(BUILD_DIR)
	$(CONVERT) $< -o $@

$(BUILD_DIR):
	mkdir -p $@

.PHONY: clean fresh
clean:
	rm -rvf $(BUILD) $(BUILD_DIR)

fresh: clean
	rm -v $(BOOK_FILENAME).*
